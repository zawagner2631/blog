# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140406172554) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "auth_group", force: true do |t|
    t.string "name", limit: 80, null: false
  end

  add_index "auth_group", ["name"], name: "auth_group_name_key", unique: true, using: :btree
  add_index "auth_group", ["name"], name: "auth_group_name_like", using: :btree

  create_table "auth_group_permissions", force: true do |t|
    t.integer "group_id",      null: false
    t.integer "permission_id", null: false
  end

  add_index "auth_group_permissions", ["group_id", "permission_id"], name: "auth_group_permissions_group_id_permission_id_key", unique: true, using: :btree
  add_index "auth_group_permissions", ["group_id"], name: "auth_group_permissions_group_id", using: :btree
  add_index "auth_group_permissions", ["permission_id"], name: "auth_group_permissions_permission_id", using: :btree

  create_table "auth_permission", force: true do |t|
    t.string  "name",            limit: 50,  null: false
    t.integer "content_type_id",             null: false
    t.string  "codename",        limit: 100, null: false
  end

  add_index "auth_permission", ["content_type_id", "codename"], name: "auth_permission_content_type_id_codename_key", unique: true, using: :btree
  add_index "auth_permission", ["content_type_id"], name: "auth_permission_content_type_id", using: :btree

  create_table "comments", force: true do |t|
    t.integer  "post_id"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "common_activityinstance", force: true do |t|
    t.datetime "start_time",               null: false
    t.datetime "end_time"
    t.string   "activity_type",  limit: 2, null: false
    t.integer  "activity_id",              null: false
    t.integer  "started_by_id",            null: false
    t.integer  "room_id",                  null: false
    t.integer  "activity_state",           null: false
    t.integer  "legacy_id"
    t.boolean  "hide_report",              null: false
  end

  add_index "common_activityinstance", ["end_time"], name: "common_activityinstance_end_time", using: :btree
  add_index "common_activityinstance", ["room_id"], name: "common_activityinstance_room_id", using: :btree
  add_index "common_activityinstance", ["started_by_id"], name: "common_activityinstance_started_by_id", using: :btree

  create_table "common_activityinstance_settings", force: true do |t|
    t.integer "activityinstance_id", null: false
    t.integer "activitysetting_id",  null: false
  end

  add_index "common_activityinstance_settings", ["activityinstance_id", "activitysetting_id"], name: "common_activityinstan_activityinstance_id_145ab688075127a2_uniq", unique: true, using: :btree
  add_index "common_activityinstance_settings", ["activityinstance_id"], name: "common_activityinstance_settings_activityinstance_id", using: :btree
  add_index "common_activityinstance_settings", ["activitysetting_id"], name: "common_activityinstance_settings_activitysetting_id", using: :btree

  create_table "common_activitysetting", force: true do |t|
    t.string "key",   null: false
    t.string "value", null: false
  end

  add_index "common_activitysetting", ["key"], name: "common_activitysetting_key", using: :btree
  add_index "common_activitysetting", ["key"], name: "common_activitysetting_key_like", using: :btree

  create_table "common_mediaresource", force: true do |t|
    t.integer "owner_id",              null: false
    t.string  "name",                  null: false
    t.string  "type",     limit: 2,    null: false
    t.string  "url",      limit: 1024, null: false
  end

  add_index "common_mediaresource", ["owner_id"], name: "common_mediaresource_owner_id", using: :btree

  create_table "common_partner", force: true do |t|
    t.integer "user_id",           null: false
    t.string  "type",    limit: 2, null: false
    t.text    "data",              null: false
  end

  add_index "common_partner", ["user_id"], name: "common_partner_user_id", using: :btree

  create_table "custom_user_emailuser", force: true do |t|
    t.string   "password",     limit: 128, null: false
    t.datetime "last_login",               null: false
    t.boolean  "is_superuser",             null: false
    t.string   "email",                    null: false
    t.boolean  "is_staff",                 null: false
    t.boolean  "is_active",                null: false
    t.datetime "date_joined",              null: false
  end

  add_index "custom_user_emailuser", ["email"], name: "custom_user_emailuser_email_key", unique: true, using: :btree
  add_index "custom_user_emailuser", ["email"], name: "custom_user_emailuser_email_like", using: :btree

  create_table "custom_user_emailuser_groups", force: true do |t|
    t.integer "emailuser_id", null: false
    t.integer "group_id",     null: false
  end

  add_index "custom_user_emailuser_groups", ["emailuser_id", "group_id"], name: "custom_user_emailuser_groups_emailuser_id_group_id_key", unique: true, using: :btree
  add_index "custom_user_emailuser_groups", ["emailuser_id"], name: "custom_user_emailuser_groups_emailuser_id", using: :btree
  add_index "custom_user_emailuser_groups", ["group_id"], name: "custom_user_emailuser_groups_group_id", using: :btree

  create_table "custom_user_emailuser_user_permissions", force: true do |t|
    t.integer "emailuser_id",  null: false
    t.integer "permission_id", null: false
  end

  add_index "custom_user_emailuser_user_permissions", ["emailuser_id", "permission_id"], name: "custom_user_emailuser_user_permi_emailuser_id_permission_id_key", unique: true, using: :btree
  add_index "custom_user_emailuser_user_permissions", ["emailuser_id"], name: "custom_user_emailuser_user_permissions_emailuser_id", using: :btree
  add_index "custom_user_emailuser_user_permissions", ["permission_id"], name: "custom_user_emailuser_user_permissions_permission_id", using: :btree

  create_table "django_admin_log", force: true do |t|
    t.datetime "action_time",                 null: false
    t.integer  "user_id",                     null: false
    t.integer  "content_type_id"
    t.text     "object_id"
    t.string   "object_repr",     limit: 200, null: false
    t.integer  "action_flag",     limit: 2,   null: false
    t.text     "change_message",              null: false
  end

  add_index "django_admin_log", ["content_type_id"], name: "django_admin_log_content_type_id", using: :btree
  add_index "django_admin_log", ["user_id"], name: "django_admin_log_user_id", using: :btree

  create_table "django_content_type", force: true do |t|
    t.string "name",      limit: 100, null: false
    t.string "app_label", limit: 100, null: false
    t.string "model",     limit: 100, null: false
  end

  add_index "django_content_type", ["app_label", "model"], name: "django_content_type_app_label_model_key", unique: true, using: :btree

  create_table "django_session", id: false, force: true do |t|
    t.string   "session_key",  limit: 40, null: false
    t.text     "session_data",            null: false
    t.datetime "expire_date",             null: false
  end

  add_index "django_session", ["expire_date"], name: "django_session_expire_date", using: :btree
  add_index "django_session", ["session_key"], name: "django_session_session_key_like", using: :btree

  create_table "django_site", force: true do |t|
    t.string "domain", limit: 100, null: false
    t.string "name",   limit: 50,  null: false
  end

  create_table "posts", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quizzes_answer", force: true do |t|
    t.integer "created_by_id", null: false
    t.text    "text",          null: false
    t.boolean "is_correct"
    t.integer "order",         null: false
    t.integer "question_id",   null: false
    t.integer "legacy_id"
  end

  add_index "quizzes_answer", ["created_by_id"], name: "quizzes_answer_created_by_id", using: :btree
  add_index "quizzes_answer", ["question_id"], name: "quizzes_answer_question_id", using: :btree

  create_table "quizzes_question", primary_key: "question_id", force: true do |t|
    t.string   "type",           limit: 2,                         null: false
    t.integer  "created_by_id",                                    null: false
    t.integer  "order",                                            null: false
    t.text     "question_text",                                    null: false
    t.text     "explanation",                                      null: false
    t.datetime "created_date",                                     null: false
    t.decimal  "grading_weight",           precision: 8, scale: 4, null: false
    t.integer  "quiz_id"
    t.integer  "legacy_id"
  end

  add_index "quizzes_question", ["created_by_id"], name: "quizzes_question_created_by_id", using: :btree
  add_index "quizzes_question", ["quiz_id"], name: "quizzes_question_quiz_id", using: :btree

  create_table "quizzes_question_resources", force: true do |t|
    t.integer "question_id",      null: false
    t.integer "mediaresource_id", null: false
  end

  add_index "quizzes_question_resources", ["mediaresource_id"], name: "quizzes_question_resources_mediaresource_id", using: :btree
  add_index "quizzes_question_resources", ["question_id", "mediaresource_id"], name: "quizzes_question_resources_question_id_2839d15d53209751_uniq", unique: true, using: :btree
  add_index "quizzes_question_resources", ["question_id"], name: "quizzes_question_resources_question_id", using: :btree

  create_table "quizzes_quiz", force: true do |t|
    t.string   "name",          null: false
    t.string   "soc_number",    null: false
    t.boolean  "is_hidden",     null: false
    t.integer  "created_by_id", null: false
    t.datetime "created_date",  null: false
    t.datetime "last_updated"
    t.boolean  "sharable",      null: false
    t.integer  "legacy_id"
  end

  add_index "quizzes_quiz", ["created_by_id"], name: "quizzes_quiz_created_by_id", using: :btree
  add_index "quizzes_quiz", ["name"], name: "quizzes_quiz_name", using: :btree
  add_index "quizzes_quiz", ["name"], name: "quizzes_quiz_name_like", using: :btree
  add_index "quizzes_quiz", ["soc_number"], name: "quizzes_quiz_soc_number", using: :btree
  add_index "quizzes_quiz", ["soc_number"], name: "quizzes_quiz_soc_number_like", using: :btree

  create_table "quizzes_quiz_tag", force: true do |t|
    t.integer "quiz_id",       null: false
    t.integer "contenttag_id", null: false
  end

  add_index "quizzes_quiz_tag", ["contenttag_id"], name: "quizzes_quiz_tag_contenttag_id", using: :btree
  add_index "quizzes_quiz_tag", ["quiz_id", "contenttag_id"], name: "quizzes_quiz_tag_quiz_id_5a4a5eb39745897e_uniq", unique: true, using: :btree
  add_index "quizzes_quiz_tag", ["quiz_id"], name: "quizzes_quiz_tag_quiz_id", using: :btree

  create_table "rooms_room", force: true do |t|
    t.string  "name",          limit: 32, null: false
    t.integer "created_by_id"
    t.integer "legacy_id"
    t.string  "name_lower",    limit: 32
  end

  add_index "rooms_room", ["created_by_id"], name: "rooms_room_created_by_id", using: :btree
  add_index "rooms_room", ["name"], name: "rooms_room_name_like", using: :btree
  add_index "rooms_room", ["name"], name: "rooms_room_name_uniq", unique: true, using: :btree
  add_index "rooms_room", ["name_lower"], name: "rooms_room_name_lower", using: :btree
  add_index "rooms_room", ["name_lower"], name: "rooms_room_name_lower_like", using: :btree

  create_table "socrative_tornado_longmessage", force: true do |t|
    t.text "message", null: false
  end

  create_table "socrative_users_socrativeuser", force: true do |t|
    t.string   "password",      limit: 128, null: false
    t.datetime "last_login",                null: false
    t.boolean  "is_superuser",              null: false
    t.string   "email",                     null: false
    t.boolean  "is_staff",                  null: false
    t.boolean  "is_active",                 null: false
    t.datetime "date_joined",               null: false
    t.date     "register_date",             null: false
    t.string   "display_name",  limit: 32,  null: false
    t.string   "auth_token",    limit: 20,  null: false
    t.string   "first_name",                null: false
    t.string   "last_name",                 null: false
    t.integer  "legacy_id"
    t.string   "language",      limit: 32,  null: false
    t.integer  "tz_offset"
  end

  add_index "socrative_users_socrativeuser", ["auth_token"], name: "socrative_users_socrativeuser_auth_token", using: :btree
  add_index "socrative_users_socrativeuser", ["auth_token"], name: "socrative_users_socrativeuser_auth_token_like", using: :btree
  add_index "socrative_users_socrativeuser", ["display_name"], name: "socrative_users_socrativeuser_display_name", using: :btree
  add_index "socrative_users_socrativeuser", ["display_name"], name: "socrative_users_socrativeuser_display_name_like", using: :btree
  add_index "socrative_users_socrativeuser", ["email"], name: "socrative_users_socrativeuser_email_key", unique: true, using: :btree
  add_index "socrative_users_socrativeuser", ["email"], name: "socrative_users_socrativeuser_email_like", using: :btree

  create_table "socrative_users_socrativeuser_groups", force: true do |t|
    t.integer "socrativeuser_id", null: false
    t.integer "group_id",         null: false
  end

  add_index "socrative_users_socrativeuser_groups", ["group_id"], name: "socrative_users_socrativeuser_groups_group_id", using: :btree
  add_index "socrative_users_socrativeuser_groups", ["socrativeuser_id", "group_id"], name: "socrative_users_socrativ_socrativeuser_id_5b4e46c31ff5d15a_uniq", unique: true, using: :btree
  add_index "socrative_users_socrativeuser_groups", ["socrativeuser_id"], name: "socrative_users_socrativeuser_groups_socrativeuser_id", using: :btree

  create_table "socrative_users_socrativeuser_user_permissions", force: true do |t|
    t.integer "socrativeuser_id", null: false
    t.integer "permission_id",    null: false
  end

  add_index "socrative_users_socrativeuser_user_permissions", ["permission_id"], name: "socrative_users_socrativeuser_user_permissions_permission_id", using: :btree
  add_index "socrative_users_socrativeuser_user_permissions", ["socrativeuser_id", "permission_id"], name: "socrative_users_socrative_socrativeuser_id_1bd34a6a80ac0ad_uniq", unique: true, using: :btree
  add_index "socrative_users_socrativeuser_user_permissions", ["socrativeuser_id"], name: "socrative_users_socrativeuser_user_permissions_socrativeuser_id", using: :btree

  create_table "south_migrationhistory", force: true do |t|
    t.string   "app_name",  null: false
    t.string   "migration", null: false
    t.datetime "applied",   null: false
  end

  create_table "students_studentname", force: true do |t|
    t.string  "name",                 limit: 128, null: false
    t.string  "user_uuid",            limit: 40,  null: false
    t.integer "activity_instance_id",             null: false
  end

  add_index "students_studentname", ["activity_instance_id"], name: "students_studentname_activity_instance_id", using: :btree

  create_table "students_studentresponse", force: true do |t|
    t.string  "user_uuid",            limit: 40, null: false
    t.boolean "is_correct"
    t.integer "activity_instance_id",            null: false
    t.integer "question_id",                     null: false
    t.integer "team",                            null: false
    t.boolean "hidden",                          null: false
    t.integer "legacy_id"
  end

  add_index "students_studentresponse", ["activity_instance_id"], name: "students_studentresponse_activity_instance_id", using: :btree
  add_index "students_studentresponse", ["question_id"], name: "students_studentresponse_question_id", using: :btree

  create_table "students_studentresponseanswerselection", force: true do |t|
    t.integer "answer_id",           null: false
    t.integer "student_response_id", null: false
  end

  add_index "students_studentresponseanswerselection", ["student_response_id"], name: "students_studentresponseanswerselection_student_response_id", using: :btree

  create_table "students_studentresponsetextanswer", force: true do |t|
    t.text    "answer_text",         null: false
    t.integer "student_response_id", null: false
  end

  add_index "students_studentresponsetextanswer", ["student_response_id"], name: "students_studentresponsetextanswer_student_response_id", using: :btree

  create_table "tags_contenttag", force: true do |t|
    t.string "type",         limit: 2,    null: false
    t.string "name",                      null: false
    t.text   "description",               null: false
    t.string "url",          limit: 1024
    t.text   "verbose_name",              null: false
  end

  add_index "tags_contenttag", ["name"], name: "tags_contenttag_name", using: :btree
  add_index "tags_contenttag", ["name"], name: "tags_contenttag_name_like", using: :btree
  add_index "tags_contenttag", ["type"], name: "tags_contenttag_type", using: :btree
  add_index "tags_contenttag", ["type"], name: "tags_contenttag_type_like", using: :btree

  create_table "tags_contenttag_grade", force: true do |t|
    t.integer "contenttag_id", null: false
    t.integer "gradelevel_id", null: false
  end

  add_index "tags_contenttag_grade", ["contenttag_id", "gradelevel_id"], name: "tags_contenttag_grade_contenttag_id_66cba9efc7558624_uniq", unique: true, using: :btree
  add_index "tags_contenttag_grade", ["contenttag_id"], name: "tags_contenttag_grade_contenttag_id", using: :btree
  add_index "tags_contenttag_grade", ["gradelevel_id"], name: "tags_contenttag_grade_gradelevel_id", using: :btree

  create_table "tags_contenttag_parent", force: true do |t|
    t.integer "from_contenttag_id", null: false
    t.integer "to_contenttag_id",   null: false
  end

  add_index "tags_contenttag_parent", ["from_contenttag_id", "to_contenttag_id"], name: "tags_contenttag_parent_from_contenttag_id_5398a3e72a9e49fd_uniq", unique: true, using: :btree
  add_index "tags_contenttag_parent", ["from_contenttag_id"], name: "tags_contenttag_parent_from_contenttag_id", using: :btree
  add_index "tags_contenttag_parent", ["to_contenttag_id"], name: "tags_contenttag_parent_to_contenttag_id", using: :btree

  create_table "tags_gradelevel", force: true do |t|
    t.string "grade", limit: 3, null: false
  end

  add_index "tags_gradelevel", ["grade"], name: "tags_gradelevel_grade", using: :btree
  add_index "tags_gradelevel", ["grade"], name: "tags_gradelevel_grade_like", using: :btree

end
